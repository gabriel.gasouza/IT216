# IT216

The examples directory contains the developed multiagent landing control scripts, in progressive complexity order.

## DEPENDENCIES

I used the following toolboxes:

* MATLAB R2022a (https://www.mathworks.com/downloads)
* Gurobi 11.01 (https://portal.gurobi.com/iam/home/)
* movie2gif (in the repo, just add to matlab path

Be careful with the yalmip master branch version. Instead, use the latest YALMIP (development branch) to make sure it works with Gurobi and MATLAB.

* YALMIP (https://github.com/yalmip/YALMIP/tree/461dd81e721ba36540f4884894126de8f1a0800c)
