%% Limpando figuras e terminal
clear;
close all;
clc;

%% Pick data 

time_limit = 300; % INPUT THIS HERE

T = readtable(['./optim_',int2str(time_limit),'.csv']);

T.Properties.VariableNames = ["Incumbent","Time"];

% to make sure it will have a line towards the end
extend_data = {T.Incumbent(end), time_limit};
T = [T;extend_data];

%% Plot

name = 'Total Cost Evolution for ';
figure('Name',name,'Position',[0 0 700 900]);
hold on;

data = stairs(T.Time, T.Incumbent);


init = plot(T.Time(1), T.Incumbent(1),'r*');

grid;
xlabel('$Time [s] $','interpreter','latex','FontSize',20);
ylabel('$Incumbent $','interpreter','latex','FontSize',20);

%xlim([0 T.Time(end)]);
xlim([0 time_limit]);
%ylim([25 100]);
ylim([0.95*T.Incumbent(end) 1.05*T.Incumbent(1)]);

title_txt = ['Total Cost Evolution for ',int2str(Nhops),'-hops']; 
sub_txt = ['Feasible at ',int2str(T.Time(1)),' [s]'];
title(title_txt,'interpreter','latex','FontSize',22);
set(gcf,'color','w');

legend(init,sub_txt,'interpreter','latex','FontSize',18);

% save
print('-depsc','-r300',['./optim_',int2str(Nhops),'_',int2str(time_limit)]);

