 classdef robot_model < handle
  % Robot model used in the optimization
  properties
    A   % State matrix
    B   % Input matrix
    Cp  % Matrix that extracts position information from state vector
    Cv  % Matrix that extracts velocity information from state vector
    Input_cons % constraints on the input variable 1 row for each variable, 2
               % columns for min and max values    
    State_cons % constraints on the state variable 1 row for each variable, 2
               % columns for min and max values
    State_ref  % reference for the state variables
    Input_ref  % reference for the input variables
    Term_vel   % Terminal velocity constraints
    x0         % Current state of the robot
    X          % Robot's predicted trajectory
    U          % Robot's predicted control sequence
    rcol       % radius for cllision avoidance
    rsafe      % safety margin for collision avoidance
    Nscol      % number of sides of the collision avoidance polytope
    Pcol       % collision avoidance polytope
    Psafe      % collision avoidance polytope with safety margin
  end
  methods  
    function obj = robot_model() % constructor method
      obj.A = [];
      obj.B = [];
      obj.Cp = [];
      obj.Cv = [];
      obj.Input_cons = [];
      obj.State_cons = [];
      obj.Input_ref = [];
      obj.State_ref = [];
      obj.Term_vel = [];
      obj.x0 = 0;
      obj.X = []; 
      obj.U = [];
      obj.rcol = [];      
      obj.rsafe = 0;    
      obj.Nscol = [];  
    end 
    function obj = evolve_state(obj,u)
      x = obj.A*obj.x0 + obj.B*u;
      obj.x0 = x;
    end
    function obj = setX(obj,X)
      obj.X = X;
    end
    function obj = setU(obj,U)
      obj.U = U;
    end
    function obj = build_collision_polytope(obj)
        if ~isempty(obj.rcol)
            if size(obj.Cp,1) == 2 % Only for dimension 2
                theta = [0:1:obj.Nscol-1]'/obj.Nscol*2*pi; %#ok<NBRAK1> 
                univec = [cos(theta), sin(theta)];
                H = [univec obj.rcol*ones(obj.Nscol,1)];
                obj.Pcol = Polyhedron('H',H);
                H = [univec (obj.rcol+obj.rsafe)*ones(obj.Nscol,1)];
                obj.Psafe = Polyhedron('H',H);
            else % dimension 3, then cube
                H = [[eye(3);-eye(3)] obj.rcol*ones(6,1)];
                obj.Pcol = Polyhedron('H',H);
                H = [[eye(3);-eye(3)] (obj.rcol+obj.rsafe)*ones(6,1)];
                obj.Psafe = Polyhedron('H',H);
            end
        end
    end
  end
end