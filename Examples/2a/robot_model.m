 classdef robot_model < handle
  % Robot model used in the optimization
  properties
    A   % State matrix
    B   % Input matrix
    Cp  % Matrix that extracts position information from state vector
    Cv  % Matrix that extracts velocity information from state vector
    Input_cons % constraints on the input variable 1 row for each variable, 2
               % columns for min and max values    
    State_cons % constraints on the state variable 1 row for each variable, 2
               % columns for min and max values
    State_ref  % reference for the state variables
    Input_ref  % reference for the input variables
    Term_vel   % Terminal velocity constraints
    x0         % Current state of the robot
    X          % Robot's predicted trajectory
    U          % Robot's predicted control sequence
  end
  methods  
    function obj = robot_model() % constructor method
      obj.A = [];
      obj.B = [];
      obj.Cp = [];
      obj.Cv = [];
      obj.Input_cons = [];
      obj.State_cons = [];
      obj.Input_ref = [];
      obj.State_ref = [];
      obj.Term_vel = [];
      obj.x0 = 0;
      obj.X = []; 
      obj.U = [];
    end 
    function obj = evolve_state(obj,u)
      x = obj.A*obj.x0 + obj.B*u;
      obj.x0 = x;
    end
    function obj = setX(obj,X)
      obj.X = X;
    end
    function obj = setU(obj,U)
      obj.U = U;
    end
  end
end