%% Limpando figuras e terminal
clear;
close all;
clc;

%% Pick data 

time_limit = 300; % INPUT THIS HERE

%T = readtable(['./optim_',int2str(time_limit),'.csv']);
T = readtable(['./incumbent','.csv']);

B = readtable(['./bestbd','.csv']);

T.Properties.VariableNames = ["Incumbent","Time"];

B.Properties.VariableNames = ["BestBound","Time"];

% to make sure it will have a line towards the end
extend_data_T = {T.Incumbent(end), time_limit};
T = [T;extend_data_T];

extend_data_B = {B.BestBound(end), time_limit};
B = [B;extend_data_B];

%% Plot

name = 'Total Cost Evolution for ';
figure('Name',name,'Position',[0 0 700 900]);
hold on;

data_T = stairs(T.Time, T.Incumbent);

data_B = stairs(B.Time, B.BestBound);

init = plot(T.Time(1), T.Incumbent(1),'r*');

grid;
xlabel('$Time [s] $','interpreter','latex','FontSize',20);
ylabel('$Value $','interpreter','latex','FontSize',20);

xlim([0 time_limit]);
ylim([1.05*B.BestBound(1) 1.05*T.Incumbent(1)]);

title_txt = 'Incumbent and Best Bound evolution'; 
sub_txt = ['Feasible at ',int2str(T.Time(1)),' [s]'];
title(title_txt,'interpreter','latex','FontSize',22);
set(gcf,'color','w');

legend([init data_T data_B],sub_txt,'Incumbent','Best Bound','interpreter','latex','FontSize',18);

% save
print('-depsc','-r300',['./optim_',int2str(time_limit)]);

