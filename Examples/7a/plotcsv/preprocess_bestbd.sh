#!/bin/bash

# $1 is input log file
# $2 is output csv file

# example: ./preprocess.sh ../results/optim_4_300.log optim_4_300.csv

# gets the bestbound, and time [s], after a feasible solution was found :-> very overfitted to the Gurobi display format
cat $1 | grep \% | grep -v "Best" | sed 's/H//g' | sed 's/\ *\ /\t/g' | sed 's/s//g' | awk '{print $(NF-3)","$(NF)}' | awk -F, '!seen[$NF]++' | tee $2

# | awk -F, '!seen[$NF]++' # grabs the first rows with unique last line element. Using "," as delimiter



