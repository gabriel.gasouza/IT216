%**************************************************************************
% Code for development - Example 3                                        *
% Constraints for ensuring multi-agent target exploration                 *
% Authors: Gabriel de Almeida Souza (gabriel.gasouza@ga.ita.br)           *
% Data: 10 June 2024                                                      *          
%**************************************************************************

% Cleaning
clear classes; %#ok<*CLCLS> 
clear all; %#ok<*CLALL> 
close all;
clc;

%**************************************************************************
%% Agent properties

T = 1; % Sample Time -- [s]
mass = 1; % agent mass -- [kg] -- assume uniform
g = 9.81; % gravity -- [m/s^2]
% Discrete Time Dynamical Model
% x(k+1) = A*x(k) + B*u(k)
% double integrator in 3d with gravity as a state
% states = r dot(r) g = x y z dot(x) dot(y) dot(z) g
A = [1 0 0 T 0 0 0; 
     0 1 0 0 T 0 0;
     0 0 1 0 0 T (-T^2/(2*mass));
     0 0 0 1 0 0 0;
     0 0 0 0 1 0 0;
     0 0 0 0 0 1 (-T/mass);
     0 0 0 0 0 0 1];
B = [T^2/(2*mass) 0 0;
     0 T^2/(2*mass) 0;
     0 0 T^2/(2*mass);
     T/mass 0 0;
     0 T/mass 0;
     0 0 T/mass;
     0 0 0];
% dimensions
nx = size(A,1); % number of states
nu = size(B,2); % number of controls

Na = 1; % Number of agents
for i = 1:Na
    % Defining agents in a loop -> useful for the next examples
    robo(i) = robot_model; %#ok<*SAGROW> % Create an agent model
    robo(i).A = A; % State Matrix
    robo(i).B = B; % Control Matrix
    robo(i).Cp = [1 0 0 0 0 0 0; 0 1 0 0 0 0 0; 0 0 1 0 0 0 0]; % Matrix that selects position from state
    robo(i).Cv = [0 0 0 1 0 0 0; 0 0 0 0 1 0 0; 0 0 0 0 0 1 0]; % Matrix that selects velocity from state
    robo(i).State_cons = [-100 100;-100 100;0 200;-20 20;-20 20;-20 20;g g]; % State Constraints
    %robo(i).Input_cons = [-10 10;-10 10;0 (2*g)]; % Control Constraints
    robo(i).Input_cons = [-100 100;-100 100;(-2*g) (10*g)]; % Control Constraints
    robo(i).State_ref = zeros(nx,1); % State reference
    robo(i).Input_ref = zeros(nu,1); % Control reference
    robo(i).Term_vel = repmat([-Inf Inf],3,1); % Terminal velocity Constraints
    %robo(i).Term_vel = [-2 2;-2 2;-2 2]; % Terminal velocity Constraints
    % interagent collision avoidance
    robo(i).rcol = 3;
    robo(i).build_collision_polytope();
end

% Initial States
robo(1).x0 = [0;0;80;0;0;0;g];

%**************************************************************************
%% Criar problema de otimização 
cenario = probHops; % Create simulation scenario object 
cenario.time_limit = 120; % Time limit [s]
% Linux path notation
cenario.log_file = ['./results/optim_',int2str(cenario.time_limit),'.log'];
% params
cenario.maxN = 40; % Attributing Maximum Horizon [Time steps]
cenario.Dim = size(robo(1).Cp,1); % Euclidian Space Dimension
cenario.Terrain = [-100 100;-100 100;0 200]; % Terrain Bounds
cenario.g = g;
cenario.Robot = robo; % Populate object
cenario.Na = Na; % Number of agents
cenario.Cost_norm = 2; % -- FUEL=1,ENERGY=2
cenario.rho = 1; % Control Weight
cenario.rho_x = 0; % State Weight
cenario.rho_time = 3; % Time Weight
cenario.rho_target = 0; % Reward for visiting targets
cenario.M = 10000; % big-M
% targets
cenario.Nts = 1; % Number of Targets
%cenario.Nts = 4; % Number of Targets
cenario.Targets{1} = [49 51;-1 1;0.2 0.6]; % 1x1 square in the air with 40 cm height, 20 cm above ground
cenario.rho_target = [0]; %#ok<*NBRAK2> % Reward Vector
cenario.T = T; % Attributing the Time Step
% Obstacles
cenario.No = 1;         % Cube: x = [20,30], y = [-20,20], z = [0,80]
obs_bounds{1} = [20 30;-20 20;0 80];
cenario.Obstacles{1} = [1 0 0 obs_bounds{1}(1,2);-1 0 0 -obs_bounds{1}(1,1);
                        0 1 0 obs_bounds{1}(2,2);0 -1 0 -obs_bounds{1}(2,1);
                        0 0 1 obs_bounds{1}(3,2);0 0 -1 -obs_bounds{1}(3,1)];


%% Resolvendo o problema de otimização
cenario.create_yalmip_prob; % Creating the Problem
cenario.solve; % Recovering the Solution
cenario.SolTime; % Solution Time -- in case you want to print
%----
Nopt = cenario.Nopt; % to use on plots

%**************************************************************************
%% Save Example Workspace
file_cenario = ['results/cenario_',int2str(cenario.SolTime),'.mat'];
save(file_cenario,"cenario");


%**************************************************************************
%% Detailed r dot(r) U plots

% Predefined color for repeatability
defined_color{1} = [0.2 0.7 0.2];
defined_color{2} = [0 0 0];
defined_color{3} = [0 0 1];
defined_color{4} = [0.6 0.2 0.2];
defined_color{5} = [1 0 1];
defined_color{6} = [0.9 0.7 0.1];
defined_color{7} = [1 0 0];

% rx
name = 'Rx';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    plot(cenario.Robot(i).X(1,1:Nopt),'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$r_x [m]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Horizontal trajectory";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_rx','epsc');
% ry
name = 'Ry';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    plot(cenario.Robot(i).X(2,1:Nopt),'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$r_y [m]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Lateral trajectory";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_ry','epsc');
% rz
name = 'Rz';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    plot(cenario.Robot(i).X(3,1:Nopt),'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$r_z [m]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Vertical trajectory";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_rz','epsc');
% dot(rx)
name = 'Rx_dot';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    plot(cenario.Robot(i).X(4,1:Nopt),'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$\dot{r}_x [m/s]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Horizontal velocity";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_rx_dot','epsc');
% dot(ry)
name = 'Ry_dot';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    plot(cenario.Robot(i).X(5,1:Nopt),'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$\dot{r}_y [m/s]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Lateral velocity";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_ry_dot','epsc');
% dot(rz)
name = 'Rz_dot';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    plot(cenario.Robot(i).X(6,1:Nopt),'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$\dot{r}_z [m/s]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Vertical velocity";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_rz_dot','epsc');
% V - absolute velocity
name = 'v_abs';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    v_abs = sqrt(cenario.Robot(i).X(4,1:Nopt).^2 + ...
             cenario.Robot(i).X(5,1:Nopt).^2 + ...
             cenario.Robot(i).X(6,1:Nopt).^2);
    plot(v_abs,'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$v_{abs} [m/s]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Absolute velocity";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_v_abs','epsc');
% ux
name = 'ux';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    plot(cenario.Robot(i).U(1,1:Nopt),'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$u_x [m/s^2]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Horizontal acceleration input";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_ux','epsc');
% uy
name = 'uy';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    plot(cenario.Robot(i).U(2,1:Nopt),'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$u_y [m/s^2]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Lateral acceleration input";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_uy','epsc');
% uz (gravity discounted)
name = 'uz';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    discounted_uz = cenario.Robot(i).U(3,1:Nopt)-g;
    plot(discounted_uz,'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$u_z [m/s^2]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Discounted hover vertical acceleration input";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_uz','epsc');
% total effort (gravity discounted)
name = 'u_abs';
figure('Name',name);
hold on;
legend('location','northeast','interpreter','latex','FontSize',16);
for i=1:cenario.Na
    legend_name = "Agent " + i;
    discounted_uz = cenario.Robot(i).U(3,1:Nopt)-g;
    u_abs = sqrt(cenario.Robot(i).U(1,1:Nopt).^2 + ...
             cenario.Robot(i).U(2,1:Nopt).^2 + ...
             discounted_uz.^2);
    plot(u_abs,'Color',defined_color{i},'DisplayName',legend_name);
end
grid;
xlabel('$time [s]$','interpreter','latex','FontSize',22);
ylabel('$u_{abs} [m/s^2]$','interpreter','latex','FontSize',22);
hold off;
xlim([0 Nopt+1]);
title_txt = "Discounted hover absolute acceleration input";
title(title_txt,'interpreter','latex','FontSize',20);
set(gcf,'color','w');
grid on;
saveas(gcf,'plots/Example3_u_abs','epsc');

%% Generate trajetory figure with obstacles and targets


name = 'Generated Trajectory';
fig_traj = figure('Name',name);
plot3(0,0,0); % added so figure understands its a 3d plot
hold on;

% Targets 
for target_num = 1:cenario.Nts
    T_lim = cenario.Targets{target_num};
% BEWARE: only works for cubes!
    Vert = [ T_lim(1,1) T_lim(2,2) T_lim(3,2);
             T_lim(1,1) T_lim(2,2) T_lim(3,1);
             T_lim(1,2) T_lim(2,2) T_lim(3,1);
             T_lim(1,2) T_lim(2,2) T_lim(3,2);
             T_lim(1,1) T_lim(2,1) T_lim(3,2);
             T_lim(1,1) T_lim(2,1) T_lim(3,1);
             T_lim(1,2) T_lim(2,1) T_lim(3,1);
             T_lim(1,2) T_lim(2,1) T_lim(3,2)];
    d = [1 2 3 4 8 5 6 7 3 2 6 5 1 4 8 7];
    TargetHandle = plot3(Vert(d,1),Vert(d,2),Vert(d,3));
end
% Obstacles
for obs_num = cenario.No
    O_lim = obs_bounds{obs_num};
% BEWARE: only works for cubes!
    Vert = [ O_lim(1,1) O_lim(2,2) O_lim(3,2);
             O_lim(1,1) O_lim(2,2) O_lim(3,1);
             O_lim(1,2) O_lim(2,2) O_lim(3,1);
             O_lim(1,2) O_lim(2,2) O_lim(3,2);
             O_lim(1,1) O_lim(2,1) O_lim(3,2);
             O_lim(1,1) O_lim(2,1) O_lim(3,1);
             O_lim(1,2) O_lim(2,1) O_lim(3,1);
             O_lim(1,2) O_lim(2,1) O_lim(3,2)];
    d = [1 2 3 4 8 5 6 7 3 2 6 5 1 4 8 7];
    ObstacleHandle = plot3(Vert(d,1),Vert(d,2),Vert(d,3));
end

%-- Starting position and trajectory

% Plot trajectory
for i=1:cenario.Na
    positions = cenario.Robot(i).Cp*cenario.Robot(i).X(:,1:Nopt+1);
    p2 = plot3(positions(1,1),positions(2,1),positions(3,1),'d','markersize',10,'linewidth',1.5,'Color', defined_color{i});
    p1 = plot3(positions(1,:),positions(2,:),positions(3,:),'*-','Color', defined_color{i});      
  
end
trajHandle = p1;
initHandle = p2; 

% generate legend

legend([TargetHandle ObstacleHandle trajHandle initHandle],'Target','Obstacle','Trajectory','Initial Position','location','northeast','interpreter','latex','FontSize',16);

axis equal;
grid;
xlabel('$r_x [m]$','interpreter','latex','FontSize',22);
ylabel('$r_y [m]$','interpreter','latex','FontSize',22);
zlabel('$r_z [m]$','interpreter','latex','FontSize',22);

%xlim([-100 100]);
%ylim([-100 100]);
%zlim([0 200]);
hold off;

title_txt = "Total Cost = " + cenario.objective_cost;
sub_txt = "Discounted Hover Energy Cost = " + cenario.final_cost; 
title(title_txt,'interpreter','latex','FontSize',20);
subtitle(sub_txt,'interpreter','latex','FontSize',18);
set(fig_traj,'color','w');
grid on;

% save
saveas(fig_traj,'plots/Example3_traj','epsc');

%% Animate
name = 'Animate Trajectory';
animate_traj = figure('Name',name);
plot3(0,0,0); % added so figure understands its a 3d plot
hold on;
% Targets 
for target_num = 1:cenario.Nts
    T_lim = cenario.Targets{target_num};
% BEWARE: only works for cubes!
    Vert = [ T_lim(1,1) T_lim(2,2) T_lim(3,2);
             T_lim(1,1) T_lim(2,2) T_lim(3,1);
             T_lim(1,2) T_lim(2,2) T_lim(3,1);
             T_lim(1,2) T_lim(2,2) T_lim(3,2);
             T_lim(1,1) T_lim(2,1) T_lim(3,2);
             T_lim(1,1) T_lim(2,1) T_lim(3,1);
             T_lim(1,2) T_lim(2,1) T_lim(3,1);
             T_lim(1,2) T_lim(2,1) T_lim(3,2)];
    d = [1 2 3 4 8 5 6 7 3 2 6 5 1 4 8 7];
    TargetHandle = plot3(Vert(d,1),Vert(d,2),Vert(d,3));
end
% Obstacles
for obs_num = cenario.No
    O_lim = obs_bounds{obs_num};
% BEWARE: only works for cubes!
    Vert = [ O_lim(1,1) O_lim(2,2) O_lim(3,2);
             O_lim(1,1) O_lim(2,2) O_lim(3,1);
             O_lim(1,2) O_lim(2,2) O_lim(3,1);
             O_lim(1,2) O_lim(2,2) O_lim(3,2);
             O_lim(1,1) O_lim(2,1) O_lim(3,2);
             O_lim(1,1) O_lim(2,1) O_lim(3,1);
             O_lim(1,2) O_lim(2,1) O_lim(3,1);
             O_lim(1,2) O_lim(2,1) O_lim(3,2)];
    d = [1 2 3 4 8 5 6 7 3 2 6 5 1 4 8 7];
    ObstacleHandle = plot3(Vert(d,1),Vert(d,2),Vert(d,3));
end
% relevant stuff
title_txt = "Total Cost = " + cenario.objective_cost;
sub_txt = "Discounted Hover Energy Cost = " + cenario.final_cost; 
title(title_txt,'interpreter','latex','FontSize',20);
subtitle(sub_txt,'interpreter','latex','FontSize',18);
set(animate_traj,'color','w');
grid on;
% plot trajectory -> adapted to generate animation
for j = 1:Nopt 
    for i=1:cenario.Na
      positions = cenario.Robot(i).Cp*cenario.Robot(i).X(:,1:Nopt+1);
      if j == 1
        p2 = plot3(positions(1,1),positions(2,1),positions(3,1),'d','markersize',10,'linewidth',1.5,'Color', defined_color{i});
      end
      p1 = plot3(positions(1,j),positions(2,j),positions(3,j),'*-','Color', defined_color{i});      
    end
    frame_sequence(j) = getframe(animate_traj);
end

%% save animation -- add to path movie2gif

movie2gif(frame_sequence, 'plots/Example3_movie.gif', 'LoopCount', 0, 'DelayTime', 0)

% run close all to close figs
%}