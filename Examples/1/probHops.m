classdef probHops < handle
    % Guidance optimization problem
    properties
        Dim       % dimension of the space (2 or 3)
        Terrain   % terrain constraints given in terms of the positions 1 row for
        % each variable, 2 columns for min and max values
        g              % gravity
        Targets        % target set constraints in terms of position
        Nts            % number of targets
        Robot          % robot model
        Na             % number of agents
        Cost_norm      % Cost used (norm 1 or 2)
        rho            % relative weight of the control in the cost
        rho_x          % relative weight of the state in the cost
        rho_target     % reward for visiting the target sets
        rho_time       % relative weight of the time in the cost
        maxN           % maximal horizon
        M              % big-M choice
        Problem        % optimization problem
        Sol            % problem solution
        SolTime        % time to compute solution
        Nopt           % optimal horizon
        time_limit     % time limit for the solver in seconds
        T              % Time step
        final_cost     % energy
        objective_cost % what you optimized for
        % Gurobi/solver specific params
        log_file       % log_file name
        log_interval   % reference period to save data [s]
        params         % gurobi params vector
        Recons         % reconstruct with yalmip vars
    end
    
    methods
        function obj = probHops() % constructor method
            obj.Dim = 2;
            obj.g = 9.81;
            obj.Terrain = [];
            obj.Targets = [];
            obj.Nts = 1;
            obj.Na = 1;
            obj.Robot = robot_model;
            obj.Cost_norm = 2;
            obj.rho = 1;
            obj.rho_x = 1;
            obj.rho_target = 0;
            obj.rho_time = 1;
            obj.M = 1000;
            obj.maxN = 10;
            obj.Problem = [];
            obj.Sol = [];
            obj.SolTime = 0;
            obj.time_limit = inf;
            obj.final_cost = inf;
            obj.objective_cost = inf;
            obj.Nopt = inf;
            obj.log_file = 'optim.log';
            obj.log_interval = 1; % [s]
        end
        
        function obj = create_yalmip_prob(obj)
            
            % Clear YALMIP problems
            yalmip('clear');
            
            % YALMIP constraints and objective function
            constraints = [];
            objective = 0;
            
            % Define binary variables related to the horizon
            b_time = binvar(ones(1,obj.maxN),ones(1,obj.maxN));
            
            %-- if this here is commented, it will always take maximum time
            for k = 1:obj.maxN
                % Time minimization is included in the cost, then the binary
                % variables are considered properly
                objective = objective + obj.rho_time*k*b_time{k};
            end
            
            % loop states
            
            for m = 1:obj.Na
                nu = size(obj.Robot(m).B,2); % size of the control vector
                nx = size(obj.Robot(m).A,1); % size of the state vector
                
                % Continuous optimization variables states and controls over prediction horizon
                U{m} = sdpvar(repmat(nu,1,obj.maxN),ones(1,obj.maxN)); %#ok<*AGROW> 
                X{m} = sdpvar(repmat(nx,1,obj.maxN+1),ones(1,obj.maxN+1));
                % binary target
                b_target{m} = binvar(obj.Nts*ones(1,obj.maxN),ones(1,obj.maxN));  
                
                % Loop up to the prediction horizon
                for k = 1:obj.maxN
                    % Objective function (obj.Cost_norm == 1 => norm 1 sum of controls and 
                    % states, obj.Cost_norm == 2 => norm 2 sum of controls and states)
                    % weights are obj.rho_x for the states and obj.rho for the controls
                    switch obj.Cost_norm
                        case 2
                            % original
                            %objective = objective + obj.rho_x*(X{m}{k})'*(X{m}{k}) + obj.rho*(U{m}{k})'*(U{m}{k});
                            % discounting gravity -- control input
                            % necessary for hover -- BEWARE: not generic!!

                            objective = objective + obj.rho_x*(X{m}{k})'*(X{m}{k}) + obj.rho*(U{m}{k}(1)*U{m}{k}(1)+U{m}{k}(2)*U{m}{k}(2)+(U{m}{k}(3)-obj.g)*(U{m}{k}(3)-obj.g));
                        case 1
                            % original
                            %objective = objective + obj.rho_x*norm(X{m}{k},obj.Cost_norm) + obj.rho*norm(U{m}{k},obj.Cost_norm);
                            % discounting gravity -- control input
                            % necessary for hover -- BEWARE: not generic!!
                    end
                    
                    % Including rewards for visiting targets
                    if obj.Nts > 1
                        for j = 1:obj.Nts
                            objective = objective - obj.rho_target(j)*b_target{m}{k}(j);
                        end
                    end
                    
                     % State, position, and control constraints are relaxed after end of the horizon
                    constraints = [constraints, obj.Robot(m).Input_cons(:,1) - obj.M*sum([b_time{1:k}])*ones(nu,1) <= U{m}{k}, U{m}{k}  <= obj.Robot(m).Input_cons(:,2) + obj.M*sum([b_time{1:k}])*ones(nu,1), ...
                                                obj.Robot(m).State_cons(:,1) - obj.M*sum([b_time{1:k}])*ones(nx,1) <= X{m}{k+1}, X{m}{k+1} <= obj.Robot(m).State_cons(:,2) + obj.M*sum([b_time{1:k}])*ones(nx,1)];
                    constraints = [constraints, obj.Terrain(:,1) - obj.M*sum([b_time{1:k}])*ones(size(obj.Robot(m).Cp,1),1) <= obj.Robot(m).Cp*X{m}{k}, ...
                                                obj.Robot(m).Cp*X{m}{k} <= obj.Terrain(:,2) + obj.M*sum([b_time{1:k}])*ones(size(obj.Robot(m).Cp,1),1)];
                    
                        
                    if k == 1 % || k == 2 % including initial conditions directly
                        constraints = [constraints, X{m}{k} == obj.Robot(m).x0];
                    end
                    %constraints = [constraints, X{m}{k+1} == obj.Robot(m).A*X{m}{k} + obj.Robot(m).B*U{m}{k}]; % replaced with:
                    constraints = [constraints, X{m}{k+1} <= (obj.Robot(m).A*X{m}{k} + obj.Robot(m).B*U{m}{k} + obj.M*sum([b_time{1:k-1}])*ones(nx,1)), ... % b_time{1:k-1} works
                                                X{m}{k+1} >= (obj.Robot(m).A*X{m}{k} + obj.Robot(m).B*U{m}{k} - obj.M*sum([b_time{1:k-1}])*ones(nx,1))];    % b_time{1:k} does not
                end % end main loop to iterate for k = 1:obj.maxN
            end % end loop to iterate for each agent
            
            % If multiple targets
            for j = 1:obj.Nts
                % Agent m visits target j at time k if binary(m,j,k) == 1
                for k = 1:obj.maxN
                    for m = 1:obj.Na
                        constraints = [constraints, obj.Targets{j}(:,1) - obj.M*(1-b_target{m}{k}(j))*ones(size(obj.Robot(m).Cp,1),1)  <= obj.Robot(m).Cp*X{m}{k+1}, ...
                                                    obj.Robot(m).Cp*X{m}{k+1} <= obj.Targets{j}(:,2) + obj.M*(1-b_target{m}{k}(j))*ones(size(obj.Robot(m).Cp,1),1)];
                    end
                end
                
                % Sum to implement 4.32i
                somaAgentesAux = 0;
                for m = 1:obj.Na
                    for k = 1:obj.maxN
                        somaAgentesAux = somaAgentesAux + b_target{m}{k}(j);
                    end
                end
                constraints = [constraints, somaAgentesAux == 1]; % mandatory! Optional if [constraints, somaAgentesAux <= 1];
                % Targets are visited before the end of the horizon
                for m = 1:obj.Na
                    somaTargetAux = 0;
                    somaTerminalAux = 0;
                    for k = 1:obj.maxN
                        somaTargetAux = somaTargetAux + k*b_target{m}{k}(j);
                        somaTerminalAux = somaTerminalAux + k*b_time{k};
                    end
                    constraints = [constraints, somaTargetAux <= somaTerminalAux];
                end
            end
            
            % Last Target
            for n = obj.Nts
                somaAgentesAllTargetsAux = 0;
                for m = 1:obj.Na
                    for k = 1:obj.maxN
                        somaAgentesAllTargetsAux = somaAgentesAllTargetsAux + b_target{m}{k}(n);
                    end
                end
                constraints = [constraints, somaAgentesAllTargetsAux == 1];
                for k = 1:obj.maxN
                    somaAgentesAllTargetsEachTimeAux = 0;
                    for m = 1:obj.Na
                        somaAgentesAllTargetsEachTimeAux = somaAgentesAllTargetsEachTimeAux + sum([b_target{m}{k}(n)]);
                    end
                    constraints = [constraints, somaAgentesAllTargetsEachTimeAux - b_time{k} == 0];
                end
            end
            
            % Terminal constraints are imposed at the end of the horizon
            % Terminal velocities
            for k = 1:obj.maxN
                constraints = [constraints, obj.Robot(m).Term_vel(:,1) - obj.M*(1-b_time{k})*ones(size(obj.Robot(m).Cv,1),1) <= obj.Robot(m).Cv*X{m}{k+1}, ...
                                            obj.Robot(m).Cv*X{m}{k+1} <= obj.Robot(m).Term_vel(:,2) + obj.M*(1-b_time{k})*ones(size(obj.Robot(m).Cv,1),1) ];
            end
 
            % Minimum time problem, impose that it ends at some point
            constraints = [constraints, sum([b_time{:}]) == 1];
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %%% Gurobi!

            ops = sdpsettings('verbose',2,'solver','gurobi'); % verbose goes from 0 to 2 (2 prints everything)
            ops.gurobi.TimeLimit = obj.time_limit;
            ops.gurobi.LogFile = obj.log_file;
            ops.gurobi.DisplayInterval = obj.log_interval;
            %ops.gurobi.WorkLimit = obj.time_limit; % does not exist on yalmip interface

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Export the model straight to a gurobi model

            [model, modelRecons, diagnostic, internalmodel] = export(constraints, objective, ops); %#ok<ASGLU> 
            model.params.WorkLimit = obj.time_limit; % [s] - solved the yalmip interface problem here
            obj.params = model.params;
            obj.Recons = modelRecons;
            gurobi_write(model,['./gurobi_model_','_',int2str(obj.time_limit),'_',int2str(obj.maxN),'.mps']);

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % solve in Gurobi
            tic;
            model = gurobi_read(['./gurobi_model_','_',int2str(obj.time_limit),'_',int2str(obj.maxN),'.mps']);
            solution = gurobi(model,obj.params);
            disp(solution);
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Save computation time
            obj.SolTime = toc;
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Convert back gurobi to Yalmip compatible modemodel
            modelRecons = obj.Recons;
            assign(recover(modelRecons.used_variables),solution.x); % recover the vars
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Recovering the results through Yalmip
            % Are obstacles present
            ops = sdpsettings('verbose',0,'solver','gurobi','usex0',1); % just to return what we have in a better format
            ops.gurobi.IterationLimit = 1; % constraint to ensure solutions doesn't change
            
            obj.Problem = optimizer(constraints, objective,ops,[],[X{:},U{:},b_time,b_target{:}]);
            
            obj.Sol = obj.Problem(); % will give a warning if used like this, but works
        end % function

        function obj = solve(obj) % will actually just get solution
            % Load positions and states for each robot            
            U_stack = [];
            for m = 1:obj.Na
                X = [obj.Sol{(m-1)*(obj.maxN+1)+1:m*(obj.maxN+1)}];
                obj.Robot(m).setX(X);
                % 
                U = [obj.Sol{obj.Na*(obj.maxN+1)+(m-1)*obj.maxN+1:obj.Na*(obj.maxN+1)+m*obj.maxN}];
                obj.Robot(m).setU(U);
                U_stack = [U_stack, U(1,:), U(2,:), U(3,:)-obj.g];
            end
            
            obj.final_cost = (obj.T*norm(U_stack, obj.Cost_norm))^2; % energy
            b_time = [obj.Sol{obj.Na*(obj.maxN+1)+obj.Na*obj.maxN+1:obj.Na*(obj.maxN+1)+obj.Na*obj.maxN+obj.maxN}]; % check
            % Determine used horizon
            obj.Nopt = round([1:obj.maxN]*b_time.'); %#ok<NBRAK1> 
            % which goal binaries are satisfied
            shift_position = obj.Na*(obj.maxN+1)+(obj.Na)*obj.maxN+obj.maxN+1; % X, U, b_time
            % sum over all instances
            goal_binaries = sum(transpose([obj.Sol{shift_position:(shift_position+obj.Na*(obj.maxN)-1)}]));
            % Post-compute objective value
            obj.objective_cost = obj.final_cost*obj.rho + obj.Nopt*obj.rho_time - goal_binaries*obj.rho_target; % objective value

        end % function
    end % methods
end % class

